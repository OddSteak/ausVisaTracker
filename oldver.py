"""script to track my visa application status"""
from time import sleep
from random import randrange
from datetime import datetime
from os import _exit
import smtplib, ssl, threading, options
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import keyring as kr
import simpleaudio as sa
from plyer import notification as toast
from msg import *

# email config
port = 465  # For SSL
context = ssl.create_default_context()
imap_server = "mail.riseup.net"
sender_email = "baasil@riseup.net"
receivers = ["baasil@riseup.net","baasilsiddiqui@gmail.com","siddiqui_m_j@yahoo.com"]

# credentials
cred_mail = kr.get_credential("riseup","sedentaryowl")
cred_immi = kr.get_credential("immi","sbaasil51@gmail.com")

# webdriver config
options=Options()
options.add_argument('-headless')
options.binary_location = r'/usr/sbin/firefox'
profile_path = r"/home/baasil/.mozilla/firefox/z93eslik.default-release"
service = Service(r"/home/baasil/dls/geckodriver")
url = 'https://online.immi.gov.au/lusc/login'
options.set_preference('profile', profile_path)

errors=0 # variable to track number of consecutive errors

def notifier(receiver_email, msg):
    """sends mail to receiver_email with msg message"""
    global imap_server, port, context, sender_email, cred_mail
    with smtplib.SMTP_SSL(imap_server, port, context=context) as server:
        server.login(cred_mail.username, cred_mail.password)
        server.sendmail(sender_email, receiver_email, msg)

def music(file):
    sa.WaveObject.from_wave_file(file).play()

# main event loop
while True:
    driver = Firefox(service=service, options=options)
    driver.get(url)
    print("------------------------------------------------------------------")
    try:
        driver.find_element(by=By.NAME, value="username").send_keys(cred_immi.username)
        driver.find_element(by=By.NAME, value="password").send_keys(cred_immi.password)
        driver.find_element(by=By.XPATH, value="/html/body/form/div/div[2]/button[2]").click() # login
        sleep(2)
        driver.find_element(by=By.NAME, value="continue").click()
        details_element = WebDriverWait(driver, 25).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="defaultActionPanel_0_7"]'))) # view details
        sleep(5)
        try:
            details_element.click()
        except:
            pass
        status_line_element = WebDriverWait(driver, 25).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="_0a1a0c1a0a0b1a0a1a0a"]/div/div/strong')))
        status_line = status_line_element.text
        trashy, status = status_line.split(": ")
        print(datetime.now().strftime("%H:%M"), end=": ")
        print(f"Application status: {status}")
        try:
            driver.find_element(by=By.ID, value="_0a1a0c1a0a0b0a1a0b0-body").click() # messages tab
            sleep(5)
            messages = driver.find_elements(by=By.XPATH, value="//table/tbody/tr") # list of messages
            latest = ''
            for i in messages:
                if '06 feb' not in i.text.lower() and '25 jan' not in i.text.lower():
                    latest_element = i; latest = i.text; break
                if 'acknowledge' not in i.text.lower():
                    latest_element = i; latest = i.text
            errors=0
        except:
            driver.save_screenshot('/home/baasil/learnpy/visa_stat/error.png')
            errors+=1
            latest=''
            messages=[]
        driver.close()

        # checking status
        if status=="Received":
            pass
        elif status=="Finalised":
            print("status change detected")
            print(latest)
            print("sending mails...")
            if 'grant' in latest.lower() or 'accept' in latest.lower():
                play = threading.Thread(target=music, args=('granted.wav',)); play.start()
                toast.notify(title="your Australian visa has been granted", message='congratulations')
                for r_mail in receivers:
                    notifier(r_mail, message_final.format(r_mail, 'granted'))
                print("mails sent")
                _exit(1)
            elif 'reject' in latest.lower():
                play = threading.Thread(target=music, args=('rejected.wav',)); play.start()
                toast.notify(title="your Australian visa has been rejected")
                for r_mail in receivers:
                    notifier(r_mail, message_final.format(r_mail, 'rejected'))
                print("mails sent")
                _exit(1)
            else:
                toast.notify(title=f"your Australian visa application status has changed to \"{status}\"")
                for r_mail in receivers:
                    notifier(r_mail, message_status.format(r_mail, status))
                print("mails sent")
                if len(messages)==2:
                    _exit(1)
        else:
            print("status change detected")
            print("sending mails...")
            toast.notify(title=f"your Australian visa application status has changed to \"{status}\"")
            for r_mail in receivers:
                notifier(r_mail, message_status.format(r_mail, status))
            print("mails sent")
            if len(messages)==2:
                _exit(1)

        # checking messages
        if len(messages)==2 and '2023' in latest:
            print("no new messages")
        elif len(messages)>2:
            print("found new message")
            print(latest)
            print("sending mails...")
            toast.notify(title="you have a new message in your Australian visa application portal", message=latest)
            for r_mail in receivers:
                notifier(r_mail, message_new.format(r_mail,latest))
            print("mails sent")
            _exit(1)
        else:
            print("can't find messages")
            sleep(5*60)
            continue

        n = randrange(15,30)
        sleep(n*60)

    except KeyboardInterrupt:
        try:
            driver.close()
        except:
            pass
        _exit(1)

    except:
        try:
            driver.save_screenshot('/home/baasil/learnpy/visa_stat/error.png')
            driver.close()
        except KeyboardInterrupt:
            _exit(1)
        except:
            pass
        print("ERROR"); errors+=1
        if errors>=5:
            notifier(receivers[0], error_message.format(receivers[0]))
            toast.notify(title="visa_stat.py",message="too many consecutive errors, exiting...")
            print("too many consecutive errors, exiting...")
            _exit(1)
        else:
            sleep(15*60)
