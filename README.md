My immi account was suspended after using this script everyday for three months not sure if that was the reason so use at your own risk. However, I was able to create a new account and imported my application with no issues. Using bots is currently not banned in immi account's [terms of use](https://immi.homeaffairs.gov.au/Pages/Conditions-of-use.aspx) I recommend checking it regularly for any updates.

There have been other unrelated instances of immi accounts being [suspended](https://www.australiaforum.com/threads/immi-account-suspended.306278/)

you are free to open issues if you need any help using this script.

PSA: if your application has exceeded [standard processing time](https://immi.homeaffairs.gov.au/visas/getting-a-visa/visa-processing-times) call DHA global service center at +61 261960196. my student visa application which had been pending for more than 5 months was granted a day after calling them.
