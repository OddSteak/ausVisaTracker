import simpleaudio as sa
from datetime import datetime
import re
from config import *

def notifier(receivers_list, msg, *format_args):
    """sends mail to receivers_list with msg message formated with rmail and *format_args"""

    for rmail in receivers_list:
        with smtplib.SMTP_SSL(IMAP_SERVER, PORT, context=CONTEXT) as server:
            server.login(CRED_MAIL.username, CRED_MAIL.password)
            server.sendmail(SENDER_EMAIL, rmail, msg.format(SENDER_EMAIL, rmail, *format_args))

def music(file):
    """plays music from wav file"""

    sa.WaveObject.from_wave_file(file).play()

def parse_date(full_message):
    """parses date from message title"""

    date_string = re.search(r"^\d{2} \w{3,4} \d{4}", full_message, re.MULTILINE).group()
    return datetime.strptime(date_string, '%d %b %Y')
