# emails

message_status = """\
From: {}
To: {}
Subject: Your Australian student visa application status has changed to \"{}\"\n\n

https://online.immi.gov.au/lusc/login
This is an automated message sent from a python script made by Baasil."""

message_new = """\
From: {}
To: {}
Subject: You have new message(s) in your australian visa application portal\n\n

{}\n\n
https://online.immi.gov.au/lusc/login
check portal to see full message, I won't do everything for you\n
This is an automated message sent from a python script made by Baasil."""

message_final = """\
From: {}
To: {}
Subject: Your Australian visa has been {}\n\n

https://online.immi.gov.au/lusc/login
check portal for details\nthis message may have been sent in error\n
this is an automated message sent from a python script made by Baasil."""

error_message = """\
From: {}
To: {}
Subject: visa_stat.py exited due to too many consecutive errors\n\n

https://online.immi.gov.au/lusc/login - check what's up here
this is an automated message"""

net_error_msg = """\
From: {}
To: {}
Subject: visa_stat.py too many consecutive net errors\n\n

https://online.immi.gov.au/lusc/login - check what's up here
this is an automated message"""

error_in_messages = """\
From: {}
To:{}
Subject: too many consecutive errors in fetching messages, from visa_stat.py\n\n

https://online.immi.gov.au/lusc/login - check what's up here
this is an automated message"""
