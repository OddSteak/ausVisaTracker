import smtplib, ssl, options
import keyring as kr
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# email config
# edit the following with the imap server configuration for your email account
PORT = 465  # For SSL
CONTEXT = ssl.create_default_context()
IMAP_SERVER = ""
SENDER_EMAIL = ""
MAILING_LIST = ["johndoe@gmail.com","johnsmith@gmail.com"] # replace with mail addresses you want to notify

# credentials
# add email and immi credentials in your keyring and edit this accordingly
# https://pypi.org/project/keyring/
CRED_MAIL = kr.get_credential("service", "username")
CRED_IMMI = kr.get_credential("","")

# webdriver config
options=Options()
options.add_argument('-headless')
options.binary_location = r'' 			# fill with firefox binary path
profile_path = r"" 				# fill with firefox profile path
SERVICE = Service(r"") 			 	# download and fill with geckodriver location
URL = 'https://online.immi.gov.au/lusc/login'
options.set_preference('profile', profile_path)

# general
ini_status = "Received" 			# replace with your current application status
ini_msg = 2             			# replace with the current number of messages in your portal
ERROR_PNG_FILENAME = "error.png"
