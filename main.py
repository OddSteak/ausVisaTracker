"""script to track my visa application status"""

from time import sleep
from random import randrange
from os import _exit
import threading
from plyer import notification as toast
from config import *
from msg import *
from utils import *

con_errors = 0 # variable to track number of consecutive errors
msg_errors = 0 # variable to track number of consecutive errors in fetching messages
net_errors = 0 # variable to track number of consecutive connection errors

# main event loop
while True:
    print("------------------------------------------------------------------")
    try:
        try:
            driver = Firefox(service=SERVICE, options=options)
            driver.get(URL)
            net_errors=0
        except:
            try:
                driver.save_screenshot('error.png')
                driver.close()
            except:
                pass
            net_errors+=1
            print("net error")
            if net_errors>=5:
                notifier(MAILING_LIST[0:1], net_error_msg)
                toast.notify(title="visa_stat.py",message="too many consecutive net errors")
                net_errors = 0
            sleep(15*60)
            continue
        driver.find_element(by=By.NAME, value="username").send_keys(CRED_IMMI.username)
        driver.find_element(by=By.NAME, value="password").send_keys(CRED_IMMI.password)
        driver.find_element(by=By.XPATH, \
                            value="/html/body/form/div/div[2]/button[2]").click() # login
        sleep(2)
        driver.find_element(by=By.NAME, value="continue").click()
        details_element = WebDriverWait(driver, 25).until(EC.element_to_be_clickable\
                          ((By.XPATH, '//*[@id="defaultActionPanel_0_7"]')))
        sleep(5)
        try:
            details_element.click()
        except:
            pass
        status_line_element = WebDriverWait(driver, 25).until(EC.visibility_of_element_located\
                              ((By.XPATH, '//*[@id="_0a1a0c1a0a0b1a0a1a0a"]/div/div/strong')))
        status_line = status_line_element.text
        trashy, status = status_line.split(": ")
        print(datetime.now().strftime("%H:%M"), end=": ")
        print(f"Application status: {status}")

        try:
            msg_tab = driver.find_element(by=By.ID, value="_0a1a0c1a0a0b0a1a0b0-body")
            while_timeout = False   # variable to deteremine if while loop timed out
            timeout_while = 0

            while not driver.find_elements(by=By.XPATH, value='//*[@id="_0a1a0c1a0a0b1a0c0a0b"]/div/div[1]'):
                trashy = msg_tab.text
                timeout_while+=1
                if timeout_while>=7:
                    msg_err = True
                    while_timeout = True
                    break
                msg_tab.click()
                sleep(10)

            if not while_timeout:
                messages = driver.find_elements(by=By.XPATH, \
                                                value="//table/tbody/tr") # list of messages
                messages_list = [i.text for i in messages]
                messages_list.sort(key=parse_date)
                new_messages_list = messages_list[ini_msg:]
                new_messages_string = "\n\n".join(new_messages_list)
                msg_errors = 0
                msg_err = False

        except:
            msg_err = True
        
        finally:
            if msg_err:
                driver.save_screenshot(ERROR_PNG_FILENAME)
                msg_errors+=1
                new_messages_string = ""
                messages=[]

            if msg_errors>=5:
                notifier(MAILING_LIST[0:1], error_in_messages)
                msg_errors = 0 

        driver.close()

        # checking status
        if status==ini_status:
            pass
        elif status=="Finalised":
            print("status change detected")
            print(new_messages_string)
            print("sending mails...", end="")
            if 'grant' in new_messages_string.lower():
                play = threading.Thread(target=music, args=('granted.wav',))
                play.start()
                toast.notify(title="your Australian visa has been granted", \
                             message='congratulations')
                notifier(MAILING_LIST, message_final, 'granted')
                print("mails sent")
                _exit(0)
            elif 'reject' in new_messages_string.lower():
                play = threading.Thread(target=music, args=('rejected.wav',))
                play.start()
                toast.notify(title="your Australian visa has been rejected")
                notifier(MAILING_LIST, message_final, 'rejected')
                print("mails sent")
                _exit(0)
            else:
                toast.notify(title=f"your Australian visa application status has changed to \"{status}\"")
                notifier(MAILING_LIST, message_status, status)
                print("mails sent")
                ini_status = status
        else:
            print("status change detected")
            print("sending mails...", end="")
            toast.notify(title=f"your Australian visa application status has changed to \"{status}\"")
            notifier(MAILING_LIST, message_status, status)
            print("mails sent")
            ini_status = status

        # checking messages
        if len(messages)==ini_msg:
            print("no new messages")
        elif len(messages)>ini_msg:
            print("found new message(s)")
            print(new_messages_string)
            print("sending mails...", end="")
            toast.notify(title="you have new message(s) in your Australian visa application portal", \
                         message=new_messages_string)
            notifier(MAILING_LIST, message_new, new_messages_string)
            print("mails sent")
            ini_msg = len(messages)
            if status=="Finalised":
                _exit(0)
        else:
            print("can't find messages")
            sleep(5*60)
            continue

        n = randrange(15,30)
        sleep(n*60)

    except KeyboardInterrupt:
        try:
            driver.close()
        except:
            pass
        _exit(1)

    except:
        try:
            driver.save_screenshot(ERROR_PNG_FILENAME)
            driver.close()
        except KeyboardInterrupt:
            _exit(1)
        except:
            pass
        print("ERROR")
        con_errors+=1
        if con_errors>=5:
            notifier(MAILING_LIST[0:1], error_message)
            toast.notify(title="visa_stat.py",message="too many consecutive errors, exiting...")
            print("too many consecutive errors, exiting...")
            _exit(1)
        else:
            sleep(15*60)
